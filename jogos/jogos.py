import forca
import adivinhacao

def escolhe_jogo():
  print("********************************")
  print("*******Escolha o seu jogo !*****")
  print("********************************")
  print("Game Over")
  print("(1) Forca")
  print("(2) Adivinhação")

  jogo = int(input("Selecione uma opção: "))

  if(jogo == 1):
    print("Jogando forca")
    forca.jogar()
  else:
    print("Jogando advinhação")
    adivinhacao.jogar()

if(__name__ == "__main__"):
  escolhe_jogo()