import random

def jogar():
  print("********************************")
  print("Bem vindo ao jogo de advinhação!")
  print("********************************")

  numero_secreto = round(random.randrange(1, 101))
  tentativas = 0
  pontos = 1000

  print("Qual o nível de dificuldade ?")
  print("(1) Fácil")
  print("(2) Médio")
  print("(3) Difícil")
  nivel = int(input("Selecione o nível de dificuldade: "))

  if(nivel == 1):
    tentativas = 20
  elif(nivel == 2):
    tentativas = 10
  else:
    tentativas = 5

  rodada = 1
  print("Você tem ", tentativas, "tentativas, cada erro custa uma tentativa")

  for rodada in range(1, tentativas + 1):
    print("Tentativa {} de {}".format(rodada, tentativas))
    
    palpite = input("Digite um número entre 1 e 100: ")

    palpite_negativo = int(palpite) < 1
    palpite_muito_alto = int(palpite) > 100
    
    if(palpite_negativo or palpite_muito_alto):
      print("Você deve digitar um número entre 1 e 100 !")
      continue
      
    acertou = int(palpite) == numero_secreto
    maior   = int(palpite) > numero_secreto

    if(acertou):
      print("Você acertou e fez {} pontos!".format(pontos))
      break
    else:
      if(maior):
        print("Você errou seu palpite foi maior do que o número secreto.")
      else:
        print("Você errou seu palpite foi menor do que o número secreto.")
      pontos_perdidos = abs(numero_secreto - int(palpite))
      pontos -= pontos_perdidos
      
  print("Game Over")

if(__name__ == "__main__"):
  jogar()