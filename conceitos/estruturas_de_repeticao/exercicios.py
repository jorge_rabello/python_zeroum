# 1 3 7 9
contador = 1
while(contador <= 10):
  print(contador)
  contador += 2
  if(contador == 5):
    contador += 2
  
  
# Conta de 1 a 10
for contador2 in range(1, 11):
  print(contador2)
  
# 1 4 7 10 - start, stop, step
for contador3 in range(1, 11, 3):
  print(contador3)