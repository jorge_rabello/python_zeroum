minha_idade = 26
idade_namorada = 25
if(minha_idade == idade_namorada):
  print("Temos idades iguais")
else:
  print("Temos idades diferentes")

numero1 = 10
numero2 = 10
if(numero1 == numero2):
  print("São números iguais")
  
idade1 = 10
idade2 = "20"
#Traceback (most recent call last):
#  File "conceitos/entrada_de_dados/exercicios.py", line 15, in <module>
#    print(idade1 + idade2)
#TypeError: unsupported operand type(s) for +: 'int' and 'str'
#print(idade1 + idade2)

nome = "Jorge"
sobrenome = "Rabello"
print(nome + sobrenome)
print(nome, sobrenome, sep=" ")