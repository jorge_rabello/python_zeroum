# Função print
print("Olá Mundo")

# Função print com separador
print("Brasil", "ganhou", 5, "títulos", "mundiais", sep=" - ")
print("Brasil", "ganhou", 5, "títulos", "mundiais", sep="\n")

# definindo e atribuindo valor para variáveis
pais = "Itália"

# verificando o tipo de uma variável <class 'str'>
print(type(pais))

quantidade_titulos = 4

# <class 'int'>
print(type(quantidade_titulos))

print(pais, "ganhou", quantidade_titulos, "mundiais");

pais = "Alemanha"

quantidade_titulos = 6

print(pais, "ganhou", quantidade_titulos, "mundiais");