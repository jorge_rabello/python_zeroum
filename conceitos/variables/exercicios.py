# Exercício 01
substantivo = "Python"

verbo = "é"

adjetivo = "fantástico"

print(substantivo, verbo, adjetivo, sep="_", end="!\n")

# Exercício 02
dia = 15
mes = 10
ano = 2021

print(dia, mes, ano, sep="/")