# Tipos de Dados - python é dinamicamente tipado !

pais = "Brasil"

#<class 'str'>
print(type(pais))

pais = 666

#<class 'int'>
print(type(pais))

pais = 7.9

#<class 'float'>
print(type(pais))

## Por convenção se utiliza snake_case para definição de nomes de variáveis em python
idade_esposa = 20
perfil_vip = 'Jorge Rabello'
recibos_em_atraso = 30
