rodada = 1
tentativas = 3

print("Tentativa {} de {}".format(rodada, tentativas))

print("Tentativa {1} de {0}".format(rodada, tentativas))

valor = 1.59
print("R$ {:07.2f}".format(valor))

valor = 4.5
print("R$ {:07.2f}".format(valor))
print("R$ {:07.2f}".format(valor))
print("R$ {:08.3f}".format(valor))

valor = 46
print("R$ {:07d}".format(valor))

dia = 5
mes = 1
ano = 1988

print("Data {:02d}/{:02d}/{:4d}".format(dia, mes, ano))